# 文豪チャット

## 概要
会話プログラム[文豪チャット](http://www.vector.co.jp/soft/winnt/amuse/se515415.html)のソースコードです。

このプログラムで使用可能なデータファイルのうち、一部のみがこのリポジトリのtext/下に収録されており、青空文庫に掲載されている文章から生成したものは[こちら](https://github.com/sakurakitten/Bungo-chat-data　)からダウンロード可能です。

## 依存ライブラリ
このプログラムでは形態素解析に[Mecab](http://taku910.github.io/mecab/)を使用しているため、ビルド時には`libmecab.lib`をリンクする必要があり、また実行時は`libmecab.dll`を実行ファイルと同じディレクトリに配置することが必要になります。
